package rush;
//rush(5,3) square
public class RushFirst {
    public static void main(String[] args){

        rush(5,3);
    }
    public static void rush(int a,int b){
        for(int i=0;i<b;i++) {
            for (int j = 0; j < a; j++) {
               if(i==0||i==b-1){
                   if(j==0||j==a-1){
                       System.out.print("o");
                   }else{
                       System.out.print("-");
                   }
               }else{
                   if(j==0||j==a-1){
                       System.out.print("|");
                   }else{
                       System.out.print(" ");
                   }
               }
            }
            System.out.println();
        }
    }
}
