package rush;

public class RushAlphaFive {
    public static void main(String[] args){
        rush(4,4);
    }
    public static void rush(int a,int b){
        for(int i=0;i<b;i++){
            for(int j=0;j<a;j++){
                if(i==0||i==b-1){
                    if(i==0&&j==0){
                        System.out.print("A");
                    }else if(i==0&&j==a-1){
                        System.out.print("A");
                    }else if(i==b-1&&j==a-1){
                        System.out.print("C");
                    }else if(i==b-1&&j==0){
                        System.out.print("C");
                    }else{
                        System.out.print("B");
                    }
                }else{
                    if(j==0||j==a-1){
                        System.out.print("B");
                    }else{
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }
}
