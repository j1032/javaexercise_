package rush;

//when calling rush(5,1), print o---o
public class RushTwo {
    public static void main(String[] args){
        rush(5,1);
    }
    public static void rush(int a,int b){

        for(int i=0;i<b;i++){
            for(int j=0;j<a;j++){
                if(j==0||j==a-1){
                    System.out.print("o");
                }else{
                    System.out.print("-");
                }
            }
            System.out.println();
        }

    }
}
