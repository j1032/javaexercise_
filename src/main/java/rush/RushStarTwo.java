package rush;

//when calling rush(5,1), *****
public class RushStarTwo {
    public static void main(String[] args){
        rush(5,1);
    }
    public static void rush(int a,int b){
        for(int i=0;i<b;i++){
            for(int j=0;j<a;j++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
