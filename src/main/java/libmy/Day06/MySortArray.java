package libmy.Day06;

import java.util.ArrayList;

//아스키코드 순대로 정렬
public class MySortArray {
    public static void main(String[] args){
        my_sort_array("hello");
    }

    public static void my_sort_array(String s){

        ArrayList<String> arr = new ArrayList<String>();
        for(int i=0;i<s.length();i++){
            char c = s.charAt(i);
           arr.add(i,String.valueOf(c)); // h,e,l,l,o
        }//for 문

        for(int i=0;i<arr.size();i++){
            String temp = "";
            if(i+1==arr.size()){
                break;
            }
            if(arr.get(i).charAt(0)>arr.get(i+1).charAt(0)){
                temp = arr.get(i);
                arr.set(i,arr.get(i+1));
                arr.set(i+1,temp);
                i--; //if문이 안걸릴 때까지 계속 돌아야 해서 i--
            }//if문

        }

        for(int i=0;i<arr.size();i++){
            System.out.print(arr.get(i)+" ");
        }


    }
}
