package libmy.Day06;

import java.util.ArrayList;
import java.util.Scanner;
//2 + 2 +사이에 띄어쓰기를 해야한다
//프로그램이 2+2라고 하나로 읽기 때문임
//+,-,%,/,*
public class DoOp {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        //String 하나하나 분리하기
        ArrayList<String> params = new ArrayList<String>();
        String temporary = "";
        for(int i=0;i<input.length();i++){
            char c = input.charAt(i);
            if (c ==' ') {
                    params.add(temporary);
                    temporary = ""; // 초기화
                    continue;
            }
            temporary += c;
        }
        params.add(temporary); //마지막 파라메터 끝에 띄어쓰기가 안들어가기 때문에 for문 끝나고 또 add해준다
        //only parsing , Find the first number, second number and operator 2+2


         int firstNumber = Integer.parseInt(params.get(0)); //first numbers change to int
         int secondNumber = Integer.parseInt(params.get(2)); //second numbers change to int

        int result = 0;
            switch(params.get(1)) {
                case "+": result = firstNumber + secondNumber;
                            break;
                case "-": result = firstNumber - secondNumber;
                            break;
                case "*": result = firstNumber * secondNumber;
                            break;
                case "/": result = firstNumber / secondNumber;
                            break;
                case "%": result = firstNumber % secondNumber;
                            break;
            }



        System.out.println(result);


    }
}
