package libmy.Day05;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MyRevParams {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        boolean temp = false;
        ArrayList<String> params = new ArrayList<String>();
        String temporary = "";
        for(int i=0;i<input.length();i++){
            char c = input.charAt(i);
            if(c==' '){
                if(temp){
                    temporary += c;
                    continue;
                }
                params.add(temporary);
                temporary =""; //initialize the string
                continue;
            }else if(c=='"'&&temp){
                temp = false;
                continue;
            }
            else if(c=='"'){
                temp = true;
                continue;
            }
            temporary += c; //캐릭터가 스트링으로 변함 temporary = temporary + "" + c
        }

        for(int i=params.size()-1;i>=0;i--){
            System.out.println(params.get(i)+"$");
        }



    }
}
