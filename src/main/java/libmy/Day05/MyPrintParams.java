package libmy.Day05;

import java.util.Scanner;
import java.util.StringTokenizer;

//./a.out test "This is a test " retest
public class MyPrintParams {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        String input = sc.nextLine();
        boolean temp = false; //트랙킹
        for(int i=0;i<input.length();i++){
             char c = input.charAt(i);
             if(c==' '){
                 if(temp){
                     System.out.print(" ");
                     continue;
                 }
                 System.out.print("$");
                 System.out.println();
                 continue; // continue 가 없으면 밑에 c가 프린트되서 space 가 들어감 , 이거 채우고 나가야함
             }else if(temp&&c=='"'){
                 temp = false;
                 continue;
             }else if(c=='"'){
                temp = true;
                continue;
             }
            System.out.print(c);
        }
    }

}



