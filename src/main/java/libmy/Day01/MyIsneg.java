package libmy.Day01;

import java.util.Scanner;

//음수 입력시 n출력, 양수나 0 입력시 p 출력
public class MyIsneg {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        my_isneg(i);
    }
    public static void my_isneg(int i){
        if(i<=0){
            System.out.println("n");
        }else{
            System.out.println("p");
        }
    }
}
