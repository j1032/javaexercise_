package libmy.Day01;

import java.util.ArrayList;

public class MyPrintCombNRealone{

    public static String join(ArrayList<Integer> list, int levelWanted) {
        String res = "";
        for (int i = 0; i < levelWanted; i++)
            res = res + list.get(i);
            return res;

    }

    public static void rec(ArrayList<Integer> list, int levelWanted, int current, int start) {
        //인덱스랑 파라메터 값이 같아지면 join을 출력 list.get(current)
        if (current == levelWanted)
            System.out.println(join(list, levelWanted));
        else
            //list.get(current)랑 levelwanted 아직 같지 않음 , for문을 계속 반복해준다
        /*
          for(int i=0;i<=7;i++){
		10-(n-2)
            for(int j=i+1;j<=8;j++){
			10-(n-1)
                for(int k=j+1;k<=9;k++) {
			10-n (parameter)
                   System.out.print(i+""+j+""+k+", ");
                }
            }
        }
        levelwanted-1-current로 설정한 이유는 , for문이 언제 닫힐지 모르기 떄문이다 ,
        10-n으로만 해주면 파라메터 값으로만 마이너스 설정이 된다 예를들어 10-n , 10-(n-1) , 10-(n-2) 이런식으로 for문을 만들어줌
        근데 , 10-(n-x)  n이 levelwanted고 -1-current 가 x이다 ,,
         */
            for (int i = start; i < 10 - (levelWanted-1-current);i++) {
                list.set(current, i);
                rec(list, levelWanted, current+1, i+1);
            }
    }

    public static void main(String []args){
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++)
            list.add(0); //if its one line {} 안해도 됌 , 10 index 들어감 value는 0
        int levelWanted = 1; //n 파라메터 아무거나 쓸 수 있음
        int current = 0; //index of digit , keep track of where you are , levelwanted랑 current랑 같은 곳에
        //있으면 recursive error가 나타나서 빠져나오지 못함 .

        if (levelWanted != 0) //1,2~~ 0이 아닌 경우
            //levelwanted-1한
            for (int i = 0; i < 10 - (levelWanted-1);i++) {
                //put digits in the list
                list.set(current, i);
                //everything in the else method
                //for문이 반복 될 수록 , index는 늘어나기 떄문에 current +1을 해줬다
                /*
                       for(int i=0;i<=7;i++){
            for(int j=i+1;j<=8;j++){
                for(int k=j+1;k<=9;k++) {
                   System.out.print(i+""+j+""+k+", ");
                }
            }
        }
        여기서 보이는 것 처럼 j는 항상 i보다 크고 , k는 j보다 크는 식으로 숫자가 점점 커진다..
        for문이 늘어날 수록 start 는 +1을 해줌
                 */
                rec(list, levelWanted, current+1, i+1);
            }
        //levelwanted 0이면 바로 이게 출력됨
        System.out.println("end~~");
    }
}