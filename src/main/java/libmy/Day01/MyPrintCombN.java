package libmy.Day01;
import java.util.ArrayList;
import java.util.Scanner;

/*Write a function that displays, in ascending order, all the numbers composed by n different digits numbers (n being
given as a parameter). Given n digits (all different), only the smallest number composed by thoses digits must be
displayed. It must be prototyped as follows
 */
//파라메터 n값을 digit수로 생각하고 0~9까지 최소값 구해보기
public class MyPrintCombN {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        my_print_combn(n);
    }
    public static void my_print_combn(int n){


        if(n<=0){
            System.out.println("0");
        }else {
            for (int i = 0; i < 10; i++) {
                if(n==1) {
                    System.out.print(i + "");
                }
                else{
                    for (int j = 0; j < 10; j++){
                        if(n==2) {
                            if (j > i) {
                                System.out.print(i + "" + j + " ");
                            }
                        }
                        else {
                            for (int k = 0; k < 10; k++) {
                                if (n == 3) {
                                    if (k > j && j > i) {
                                        System.out.print(i + "" + j + "" + k + " ");
                                    }
                                } else {
                                    for (int x = 0; x < 10; x++) {
                                        if (n == 4) {
                                            if (x > k && k > j && j > i) {
                                                System.out.print(i + "" + j + "" + k + "" + x + " ");
                                            }
                                        }
                                        else {
                                            for (int y = 0; y < 10; y++) {
                                                if (n == 5) {
                                                    if (y > x && x > k && k > j && j > i) {
                                                        System.out.print(i + "" + j + "" + k + "" + x + "" + y + " ");
                                                    }
                                                }
                                                else {
                                                    for (int z = 0; z < 10; z++) {
                                                        if (n == 6) {
                                                            if (z > y && y > x && x > k && k > j && j > i) {
                                                                System.out.print(i + "" + j + "" + k + "" + x + "" + y + "" + z + " ");
                                                            }
                                                        }
                                                        else {
                                                            for (int a = 0; a < 10; a++) {
                                                                if (n == 7) {
                                                                    if (a > z && z > y && y > x && x > k && k > j && j > i) {
                                                                        System.out.print(i + "" + j + "" + k + "" + x + "" + y + "" + z + "" + a + " ");
                                                                    }
                                                                }
                                                                else {
                                                                    for (int b = 0; b < 10; b++) {
                                                                        if (n == 8) {
                                                                            if (b > a && z > y && y > x && x > k && k > j && j > i) {
                                                                                System.out.print(i + "" + j + "" + k + "" + x + "" + y + "" + z + "" + b + " ");
                                                                            }
                                                                        }
                                                                        else {
                                                                            for (int p = 0; p < 10; p++) {
                                                                                if (n == 9) {
                                                                                    if (p > b && z > y && y > x && x > k && k > j && j > i) {
                                                                                        System.out.print(i + "" + j + "" + k + "" + x + "" + y + "" + z + "" + p + " ");
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    for (int o = 0; o < 10; o++) {
                                                                                        if (n == 10) {
                                                                                            if (o > p && z > y && y > x && x > k && k > j && j > i) {
                                                                                                System.out.print(i + "" + j + "" + k + "" + x + "" + y + "" + z + "" + o + " ");
                                                                                            }
                                                                                        }

                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }

                                }
                            }
                        }


                    }

                }



            }



        }


    }
}
//not good at all