package libmy.Day01;

/*
숫자 두개로 각각 다른 숫자 만들어보기
Write a function that displays, in ascending order, all the different combinations of two two-digit numbers (00 01,
00 02, 00 03, 00 04, 00 05,...,01 99, 02 03, ..., 98 99). It must be prototyped as follows:
 */
public class MyPrintComb2 {
    public static void main(String[] args) {
        my_print_comb2();
    }
    public static void my_print_comb2(){

        /*
        * 두번째 숫자 넣는 곳은 항상 1이 먼저 시작해서 j+1해줌
        * 근데 98 99 라서 j는 항상 i보다 커야함 ..
        * */
        for(int i=0;i<=99;i++){
            for(int j=i+1;j<=99;j++){
                if(j>i)
                {
                    String formatted = String.format("%02d %02d ", i, j);
                    System.out.print(formatted);
                }
            }
        }
    }

}
