package libmy.Day01;


//숫자 3개 반복해서 최소값 내기
public class MyPrintComb {
    public static void main(String[] args){
        my_print_comb();
    }
    public static void my_print_comb() {

        /*
        //0부터 7까지 숫자가 7번 바뀜 (숫자가 점점 작아지는 이유는 , 최소값을 뽑아내야해서 , 항상 겉에
        있는 digit이 작아야 한다)
        for(int i=0;i<=7;i++){
            //012부터 089까지 8번 숫자가 바뀜 , 두번째 digit 설정
            for(int j=i+1;j<=8;j++){
                //012부터 789까지 8번 숫자가 바뀜 , 마지막 digit을 k로 설정
                for(int k=j+1;k<=9;k++) {
                   System.out.print(i);
                    System.out.print(j);
                    System.out.print(k);
                    System.out.print(", ");
                }
            }
        }

         */

        //789 i는 j,k보다 값이 항상 작기 때문에 8,9로 갈 수 없음 -> 큰 틀 row 7번 반복
        for(int i=0;i<10;i++){
            //j는 9보다 크지만 , k보다 항상 값이 작아야 하기 때문에 k의 최대치인 9밑인,8에서 끝남 -> 뉴라인 8번
            for(int j=0;j<10;j++){
                //k는 모든 최대치를 다 가질 수 있어서 9 -> 마지막 컬룸값 9..
                for(int k=0;k<10;k++){
                    if(j>i && k>j){
                        System.out.print(i+""+j+""+k+", ");
                    }
                }
            }
        }






   }
}

