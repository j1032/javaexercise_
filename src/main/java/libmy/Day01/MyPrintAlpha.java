package libmy.Day01;

import java.util.ArrayList;




public class MyPrintAlpha {
    public static void main(String[] args) {

        //소문자 알파벳 오름차순으로 정렬
        my_print_alpha();
        System.out.println();
        //소문자 알파벳 내림차순으로 정렬
        my_print_revalpha();
        System.out.println();
        //char로 숫자 0~9 프린트
        my_print_digits();

    }

    public static void my_print_alpha() {

        int i = 97;
        while(true){
            System.out.print((char)i);
            i++;
            if(i==123){
                break;
            }
        }

    }

    public static void my_print_revalpha() {

      /*
        *case 1
        int i = 122;
        while(true){
            System.out.print((char)i);
            i--;
            if(i==96){
                break;
            }
        }

       */

        //case2
        for(char i='z';i>='a';i--){
            System.out.print(i);
        }
    }

    public static void my_print_digits(){

        for(char i='0';i<='9';i++){
            System.out.print(i);
        }
    }
}

