package libmy.Day04;


import java.io.BufferedReader;
import java.util.ArrayList;

//string값 반대로 출력
public class MyRevStr {
    public static void main(String[] args){
        String input = "hi";

       for(int i=input.length()-1;i>=0;i--){
           char c = input.charAt(i);
           System.out.print(c);
       }
    }
}
