package libmy.Day04;


import java.util.ArrayList;

//십진수에서 이진수 변환
public class DecimalToBinary {
    public static void main(String[] args){
        DecimalTo_Binary(19);
    }
    public static void DecimalTo_Binary(int n){
        int remainder = 0;
        ArrayList<Integer> remains = new ArrayList<Integer>();
        //몫이 2밑으로 내려갈때까지 한다 , n이 1이나 0나올 때 while문을 나옴
        while(n>=2){
            remainder = (n%2); //19/2 ->1, 9/2 -> 1, 4/2 ->0,2/2 ->0
            remains.add(remainder); //1,1,0,0
            n = (n/2); // 9,4,2,1
        }
        remains.add(n);

        for(int i=remains.size()-1; i>=0 ; i--){
            System.out.print(remains.get(i));
        }

    }
}
