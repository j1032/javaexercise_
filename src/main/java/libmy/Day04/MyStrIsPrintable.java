package libmy.Day04;
//Write a function that returns 1 if the string passed as parameter
// only contains printable characters and 0 otherwise
public class MyStrIsPrintable {
    public static void main(String[] args){

        my_str_is_printable("fsslf");
    }

    public static void my_str_is_printable(String str){
        boolean temp = false;
        for(int i=0;i<str.length();i++){
            char c = str.charAt(i);
            int charToint = c;
            if(32<charToint&&charToint<127){
                if(i+1==str.length()) {
                    System.out.println(1);
                }
                temp = true;
            }else{
                System.out.println(0);
                temp = true;
                if(temp)
                    break;
            }
        }

    }

}
