package libmy.Day02;


import java.util.ArrayList;

//array 오름차순으로 출력하기 asc 저 -> 고 (1,2,5,7 )
//Write a function that sorts an integer array in ascending order, given a pointer to the first element of the array and its
//size
public class MySortIntArray {
    public static void main(String[] args){
        ArrayList<Integer> digits = new ArrayList<Integer>();
        digits.add(2);
        digits.add(5);
        digits.add(1);
        digits.add(7);

        my_sort_int_array(digits);
    }
    public static void my_sort_int_array(ArrayList<Integer> a){
        //2517
        for(int i = 0; i < a.size(); i++){
            int temp = 0;
            //a.get(3)까지 체크가 가능함
            //근데 밑에 if문에 a.get(i+1)을 만들어 놓음  만약 i가 3일 때 if문이 계속 돌아가면
            //a.get(4)도 검사해야함 . 범위 밖임 .. 따라서 i+1을 해줌
            if(i+1==a.size()){
                break;
            }
            //2,5,1,7
            if(a.get(i)>a.get(i+1)){
            temp = a.get(i); // 5 ,, 2
            a.set(i, a.get(i+1)); // 1,1 ,,0,1
            a.set(i + 1, temp); // 2, 5 -> 2,1,5 7 ,, 1,2,5,7
            //i는 항상 i++로 되기 떄문에 위로에서 내려올 떄 1임 , i를 -1로 해줘야 초기화됨
                //a.get(0)상태로 돌아가서 if문 컨디션이 맞을 때까지 계속 돌아준다.
            i=-1;
            }
        }



        for(int i = 0; i < a.size(); i++){
            System.out.print(a.get(i)+" ");
        }



    }
}
