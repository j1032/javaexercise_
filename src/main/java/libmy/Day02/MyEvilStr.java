package libmy.Day02;

import libmy.Day01.MyIsneg; //패키지를 다 가져오면 전체 클라스를 쓸 수 있음

import java.util.Scanner;

//import static libmy.Day01.MyIsneg.my_isneg; 이때는 static 메소드만 가져오는거 ,

//받은 문자열 반대로 출력하기 ab->ba , abc -> cba...
public class MyEvilStr {
    public static void main(String[] args){

        //MyIsneg.my_isneg(2);

        Scanner sc = new Scanner(System.in);
        String evilsStr = sc.nextLine();
        my_evil_str(evilsStr);

    }
    public static void my_evil_str(String evilsStr){

        int n = evilsStr.length();
        //char c1 = evilsStr.charAt(n-1); <-if you do it outside of loop, it gets only one character

        for(int i= n-1; i>=0; i--){
            char result = evilsStr.charAt(i);
            System.out.print(result);

        }

    }
}
