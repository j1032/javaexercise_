package libmy.Day02;


import java.util.Scanner;

//스왑하기
public class MySwap {
    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        my_swap(a,b);

    }
    public static void my_swap(int a,int b){

        int temp = 0;
        temp = a;
        a = b;
        b = temp;
        System.out.println(a+""+b);

    }
}
