package libmy.Day02;

import java.util.ArrayList;

//int아닌 에러에서 멈추기
public class MyGetnbr {
    public static void main(String[] args){
        my_getnbr("478jk8");
    }
    public static void my_getnbr(String getnbr){

        boolean temp = false;
        //string을 char로 하나하나 바꿈
        for(int i=0;i<getnbr.length();i++) {
            char c = getnbr.charAt(i);
            //-의 갯수만 따로 확인
            int count = 0;
            for(int j=0;j<getnbr.length();j++) {
                if(c=='-'){
                    count++;
                }
            }
            //-가 음수일 경우 -를 출력
            if(count%2==1){
                System.out.print("-");
            }
            //digit 0에서 9가 나올 때 출력
            //digit이 나오면 , c를 프린트하고 temp 값이 true로 바뀜
            //temp를 써준 이유는 , for문에서 완전히 나가기 위함임 , 만약 숫자가 프린트되지 않는다면 , else로 가게 되는데
            //이때 temp가 없으면 다시 for문 처음부터 시작하지만 , else문에 temp가 true라서 for문을 완전히 나가게 된다.
            if (c>='0'&&c<='9') {
                System.out.print(c);
                temp = true;
            }
           else{
               if(temp)
                   //여기서 나가면 for룹에서 완전히 나가게 된다 .
                break;
            }
        }
    }
}