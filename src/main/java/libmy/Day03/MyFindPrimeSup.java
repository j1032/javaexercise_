package libmy.Day03;
//파라메터 값에서 같거나 큰 가장 작은 소수를 구하라
//Write a function that returns the smallest prime number that is greater than, or equal to, the number given as a parameter.
//5를 출력하면 5가 나오고 , 6을 출력하면 7이 나온다.
public class MyFindPrimeSup {
    public static void main(String[] args){
        int answer = my_find_prime_sup(9); //답 11나와야 함
        System.out.println(answer);
    }
    //prime number = 1이랑 n으로만 나누어지는 숫자
    public static int my_find_prime_sup(int n){
        int result = n;
        //0,1
        if(n<2){
            result = 2;
        }
        int wholenumber = 0;
        for(int i=2;i<n/2;i++){
            if(n%i==0) {
                wholenumber = n; //8
                break;
            }
        }
        int closestPrimeNumber = 0;
        for(int i=2;i<n;i++){
            closestPrimeNumber = wholenumber;
            if(wholenumber%i==0){
                wholenumber++;
            }

            result = closestPrimeNumber;
        }


        return result;
    }
}
