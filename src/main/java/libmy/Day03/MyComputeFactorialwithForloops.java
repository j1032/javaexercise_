package libmy.Day03;


public class MyComputeFactorialwithForloops {
    public static void main(String[] args){

       int answer = my_compute_factorial_for(5);
        System.out.println(answer);

    }
    public static int my_compute_factorial_for(int n){

        //3*2*1
        //n*(n-1)*(n-2)
        //n*(n-i)*(n-i)
        int result = 1;
        if(n==1){
            result = 1;
        }
        else{
            //0,1,2
            for(int i=0;i<n;i++){
                result = result * (n-i);
            }
        }

        return result;
    }

}
