package libmy.Day03;

//거듭제곱 만들기
//Write an iterative function that returns the first argument raised to the power p, where p is the second argument.
//It must be prototyped the following way:

public class MyComputePowerIt {
    public static void main(String[] args){

        my_compute_power_it(2,3);



    }
    public static void my_compute_power_it(int n,int p) {
        //2*2
        //3*3
        //4*4*4*4
        int result = n;
        int number =1;
        while(number != p){
            number++;//1
            result = result*n;
            //4 = 2*2 number  = 1
            //8 = 4 * 2 number =2
        }
        System.out.println(result);

    }
}
