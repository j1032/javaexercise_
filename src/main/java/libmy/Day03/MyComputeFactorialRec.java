package libmy.Day03;


public class MyComputeFactorialRec {
    public static void main(String[] args){

        int answer = my_computer_factorial_rec(3);
        System.out.println(answer);

    }
    public static int my_computer_factorial_rec(int n) {
        //3
        int result = 0;
        if (n > 1){
            //function 만들 때, function을 하나씩 더 만들어준다 .. 그니까 다른 칸을 하나씩 만드는 것임
            //3*function(2)에 있을 때, 3은 그대로 남아있고, 이제 function(2)박스안으로 들어가는 것이다
            //이떄 n이 일로 되면 그때 function이 끝나게 되서 , 위에 있던 다른 메소드들을 계산해준다.

            result = n * my_computer_factorial_rec(n-1);
        }else {
            result = 1;
        }

        return result;

    }
}
