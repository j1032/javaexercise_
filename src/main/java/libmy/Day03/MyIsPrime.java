package libmy.Day03;

import java.util.ArrayList;

// 소수(prime number)면 1출력 , 소수가 아니면 0
public class MyIsPrime {
    public static void main(String[] args){
     int answer = my_is_prime(5);
        System.out.println(answer);
    }
    //답 1
    public static int my_is_prime(int n) { //5
//소수면 1 , 소수가 아니면 0
        int result = 1;

        //숫자는 0으로 나눌 수 없음,
        //소수는 1이랑 자기 자신만 나눌 수 있음
        //i으로 나누어지면 소수가 아님
        //자기 자신과 , 1로 나누어지면 소수임 .
        //따라서 2부터 자기자신 -1 을 넣었을 때 나누어 떨어지면 소수가 아니다 . i<n으로 써도 됌
        for(int i=2;i<=n-1;i++){
            if(n%i==0){
                result = 0;
                break;
            }
        }
            return result;
    }

}
