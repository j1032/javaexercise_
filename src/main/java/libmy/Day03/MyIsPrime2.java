package libmy.Day03;

public class MyIsPrime2 {
    public static void main(String[] args){

        int answer = MyIsPrime2(33);
        System.out.println(answer);
    }

    public static int MyIsPrime2(int n){
        int result = 1;


        //
        for(int i=2;i<=n/2;i++){
            /*
            n이 합성수인지 알아보기
            1.n/i == 0  .. ( i의 범위는 2에서 n-1)가 나눠줘서 나머지가 안나오면 합성수이다.
            2.n%i에 나머지가 안나오면 합성수이다.
            3.n/i를 해줘서 나머지가 0이 나오는 경우를 구해주는 것인데,
            4.이때 i의 범위를 2에서 n/2로 해줌..
            41을 체크한다고 생각해보자
            2에서 41까지 다 체크 할 수 있음, 근데 한 숫자만 확인해서 , 절반만 선택해서 확인해줘도 괜찮음 ,
            예를 들어 i의 범위를 2에서 41/2 = 20.50000라고 치자 ( 딱 절반)
            이때 i는 i++때문에 정수만 쓸 수 있음 , 즉 41/20 까지 가능
             41을 1부터 20까지 나누면 20.xxx~2.xxx 의 숫자가 나온다 ,
             그 뒤로는 앞자리 숫자가 1.xxx로 바뀌는데 , 앞에서 2자리의 숫자까지 0으로 떨어지는게
            이때는 2에서 1로 넘어가는 거라서 테스트 할 필요가 없음
              */
            if(n%i==0){
                //합성수, 나누어 떨어짐
                result = 0;
                break;
            }


        }
        return result;
    }
}
