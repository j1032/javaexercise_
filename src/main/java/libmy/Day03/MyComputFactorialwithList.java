package libmy.Day03;

import java.util.ArrayList;

public class MyComputFactorialwithList {
    public static void main(String[] args){

       int answer =  my_comput_factorial(5);
        System.out.println(answer);
    }
    public static int my_comput_factorial(int n) {
        int result = 1;
        //4*3*2*1  n*(n-1)*(n-2)*(n-3)
        //3*2*1    n*(n-1)*(n-2)
        //2*1      n*(n-1)
        ArrayList<Integer> list = new ArrayList<Integer>();
        for(int i=0;i < n; i++){
             //n*(n-i);
            // 4*3*2*!
            //4*(4-1) = 4*3
            //4*(4-2) = 4*2
            //4*(4-3) = 4*1
            list.add(n-i) ; // 4,3,2,1
        }

        for(int i=0; i<list.size(); i++){

            result *= list.get(i);
        }



        return result;
    }
}
