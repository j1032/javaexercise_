package libmy.Day03;

//recursive call로 제곱근 만들어보기
public class MyComputePowerRec {
    public static void main(String[] args){

        int answer = my_compute_power_rec(3,2);
        System.out.println(answer);

    }
    public static int my_compute_power_rec(int n,int p){
        //3*3
        int result = n;
        if(p>1) {
            p--; // 1
            result = result * my_compute_power_rec(n, p); //
            //result = 3*power(3,1) -> 3*3
        }else{
            result = n;
        }


        return result; //2*power(2,2) , 2*power(2,1) , 1


    }
}
