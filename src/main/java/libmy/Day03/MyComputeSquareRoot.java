package libmy.Day03;

//바로 떨어지는 루트 구하기 brute-force 무차별대입공격
//n/x = x x를 찾아야 함 ..
public class MyComputeSquareRoot {
    public static void main(String[] args){
      int answer = my_compute_square_root(19);
        System.out.println(answer);
    }
    public static int my_compute_square_root(int n) {
        //root4 -> 2*2  = > 2;
        int result = 0;

        //루트가 떨어지지 않는 경우, 0을 반환해야 한다.
        //이때 int로만 계산하면 답이 항상 떨어지기 떄문에 ,
        for(int i=1;i<n;i++){
            //다른 식에 flaot을 캐스팅하면 에러가 났음
            //float == integer처럼 타입을 비교할 수는 있지만 float=integer로 답을 줄 수는 없다 .
            /*
            comparison works same as computation, if you have only integer,
            it will work with integer,
             but if you have at least one variable that is a float,
            all variables with become float while you do the computation or comparison
             */
            //n이 17이라면 , 17/4.00 = 4.00 으로 비교가 되는 것이다. 따라서 if문을 충족 x for문을 빠져나가게 된다.
            if(n/(float)i==i) {
                //i는 if스콥 안에서만 float이고 스콥을 벗어면 int 인 상태이다 .
                /*when you do a cast to another type
                it stay an integer, but for this line it will use another type
                 */
                result = i;
                break;
            }
        }
        //for룹에서 빠져나가면 디폴트 값으로 정해두었던 0이 들어가게 된다
      return result;
    }
}
